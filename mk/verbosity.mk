V ?= 0

v_0              = @
v_2              = set -x;
v_stdout_0       = > /dev/null
v_stderr_0       = 2> /dev/null
v_SWU_flag_0     =
v_SWU_flag_1     = --loglevel INFO
v_SWU_flag_2     = --loglevel DEBUG
v_easyrsa_flag_0 = -s -S

v              = $(v_$(V))
v_stdout       = $(v_stdout_$(V))
v_stderr       = $(v_stderr_$(V))
v_SWU_flag     = $(v_SWU_flag_$(V))
v_easyrsa_flag = $(v_easyrsa_flag_$(V))

define get-verbose-msg
    $(foreach var,$(1),
        $(eval v_$(var)_0 = @printf '[make %-9s] %s\n' $(var) $$$$(patsubst $$$$(TOP_DIR)%,%,$$$$@) ;)
        $(eval v_$(var)_2 = set -x;)
        $(eval v_$(var)   = $(v_$(var)_$(V)))
    )
endef
