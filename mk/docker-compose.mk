# help: conatainer up
up: image-check
	$(v)cd $(TOP_DIR); docker-compose up -d
.PHONY: up

# help: conatainer up no fork
up-debug: image-check
	$(v)cd $(TOP_DIR); docker-compose up
.PHONY: up-debug

# help: conatainer down
down:
	$(v)cd $(TOP_DIR); docker-compose down
.PHONY: down

restart: image-check
	$(v)cd $(TOP_DIR); docker-compose restart
.PHONY: restart

# help: conatainer logs
logs:
	$(v)cd $(TOP_DIR); docker-compose logs -f
.PHONY: logs

# help: show container status
status ps:
	$(v)cd $(TOP_DIR); docker-compose ps
.PHONY: ps
