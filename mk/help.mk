# NOTE: if target name compose from variable,
# this variable should be exported by `export`
define gen-targets-help
    @sed -n -e 's/^# help-section: \(.*\)/\nprintf "\\n### %s\\n" "\1"/;t z4' \
            -e 's/^# help: \(.*\)/\1/;t z2' -e 'b z1' -e ':z2' -e N              \
            -e 's/\$$(\([^)]*\))/$${\1}/g'                                \
            -e 's/\(.*\n[^:]*\):.*/\1/'                                   \
            -e ':z3;s/ \([^\n ]*\)$$/,\1/; tz3;'                          \
            -e 's/\(.*\)\n\(.*\)/printf "%-20s - %s\\n" "\2" "\1"/;t z4; b; :z4; H;'      \
            -e ':z1; $${g;:z5' \
            -e 's/\(printf[^\n]*### [^\n]*\n\)\(.*\)\(\1\)\(\([^\n]\{1,\}\n\)\{1,\}\)\n/\1\4\2/; tz5;p}' \
                 $1 | sh
endef

help-targets:
	@echo 'Targets:'
	$(call gen-targets-help, $(MAKEFILE_LIST))
.PHONY: help-targets
