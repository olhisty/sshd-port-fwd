MAKEFILE := $(abspath $(firstword $(MAKEFILE_LIST)))
TOP_DIR  := $(dir $(MAKEFILE))

DOCKER_CONTAINER=sshd-port-fwd
DOCKER_IMAGE = olha-team/$(DOCKER_CONTAINER)

# Cmdline to run docker. 
DOCKER_VOLUMES = -v $(TOP_DIR)/root:$(DOCKER_WORK_DIR)

include mk/docker-container.mk
include mk/docker-compose.mk
include mk/docker-image.mk
include mk/verbosity.mk
include mk/help.mk

.DEFAULT_GOAL := help

# help-section: Information
# help: Show this help
help: help-targets
	@echo ''
.PHONY: help-local

$(DOCKER_IMAGE_ARTIFACTS):
	@mkdir $(ARTIFACTS_DIR)
	@cd $(ARTIFACTS_DIR); wget --check-certificate=off $(DOCKER_IMAGE_ARTIFACTS_URLS)

# help: shell insude container
shell:
	docker-compose exec $(DOCKER_CONTAINER) bash
.PHONY: shell

# help: reload ssh config
reload:
	docker-compose exec $(DOCKER_CONTAINER) pkill -SIGHUP sshd
.PHONY: reload
