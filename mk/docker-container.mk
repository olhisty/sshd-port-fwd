DOCKER_WORK_DIR ?= /work
DOCKER_VOLUMES  ?= -v $(TOP_DIR):$(DOCKER_WORK_DIR)
DOCKER_RUN      ?= docker run -it --rm -w $(DOCKER_WORK_DIR) \
        $(DOCKER_VOLUMES)                             \
        -v $(HOME)/.bash_history:/root/.bash_history  \
        $(DOCKER_IMAGE)

devshell: image-check
	$(v)$(DOCKER_RUN) bash
.PHONY: devshell

