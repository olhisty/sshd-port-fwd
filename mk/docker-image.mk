# help-section: Working with docker image
# help: Check if dockerd is running
docker-check:
	@docker ps > /dev/null
.PHONY: docker-check

# help: Checking exising docker image, and if not - build
image-check: docker-check
	@if ! docker inspect $(DOCKER_IMAGE) > /dev/null 2>&1; then \
        echo WARNING: docker image $(DOCKER_IMAGE) do not exist. Build one>&2; \
        $(MAKE) image-build; \
    fi
.PHONY: image-check

# help: Build docker image
image-build: $(DOCKER_IMAGE_ARTIFACTS)
	$(v)cd docker && docker build -t $(DOCKER_IMAGE) .
.PHONY: image-build

# help: Clean docker image and containers
image-clean: docker-check
	$(v)docker container ls | awk '"$(DOCKER_IMAGE)$(suffix $@)" == $$1 {print $$1":"$$2}' | \
        xargs --no-run-if-empty docker container rm
	$(v)docker inspect --type=image $(DOCKER_IMAGE) > /dev/null 2>&1 && \
        docker image rm $(DOCKER_IMAGE)$(suffix $@) || exit 0
.PHONY: image-clean
